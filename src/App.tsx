import "./App.css";
import Header from "./components/header/Header";
import Characters from "./components/characters/Characters";

const App = () => {
  return (
    <>
      <Header />
      <Characters />
    </>
  );
};

export default App;
