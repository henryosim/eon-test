import { render, screen, waitFor } from "@testing-library/react";
import Header from "./Header";

test("rending of header", async () => {
  const { getByText } = render(<Header />);

  expect(getByText("Rick and Morty GraphQL App")).toBeInTheDocument();
});
