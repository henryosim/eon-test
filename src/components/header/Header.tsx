import React from "react";

export default function Header() {
  return (
    <header>
      <h1>Rick and Morty GraphQL App</h1>
    </header>
  );
}
