import { CharacterInterface } from "../queries";
import "./Character.css";
interface CharacterProp {
  character: CharacterInterface;
  openModal: (character: CharacterInterface) => void;
}
const Character = ({ character, openModal }: CharacterProp) => {
  return (
    <div className="responsive" onClick={() => openModal(character)}>
      <div className="character">
        <img src={character.image} alt={character.name} />
        <div className="name">{character.name}</div>
      </div>
    </div>
  );
};

export default Character;
