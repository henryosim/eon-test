import React from "react";
import ReactModal from "react-modal";
import { CharacterInterface } from "../queries";
import "./Modal.css";

interface ModalProp {
  character: CharacterInterface | null;
  isOpen: boolean;
  onRequestClose: () => void;
}

ReactModal.setAppElement("body");

export default function Modal({
  character,
  isOpen,
  onRequestClose,
}: ModalProp) {
  return (
    <ReactModal
      isOpen={isOpen}
      onRequestClose={onRequestClose}
      className="modal"
    >
      <button className="close" onClick={onRequestClose}>
        <small>Close</small>
      </button>
      {character ? (
        <>
          <p>
            <small>Name:</small>
            <br />
            <strong>{character.name} </strong>
          </p>

          <p>
            <small>Status:</small>
            <br />
            <strong>{character.status}</strong>
          </p>

          <p>
            <small>Species:</small>
            <br />
            <strong>{character.species}</strong>
          </p>

          <p>
            <small>Location:</small>
            <br />
            <strong>{character.location.name}</strong>
          </p>
        </>
      ) : (
        "Oops! No content to display"
      )}
    </ReactModal>
  );
}
