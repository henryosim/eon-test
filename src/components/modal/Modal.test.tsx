import React, { useEffect } from "react";
import { render, fireEvent } from "@testing-library/react";
import Modal from "./Modal";

const modalRoot = document.createElement("div");
modalRoot.setAttribute("id", "modal-root");
document.body.appendChild(modalRoot);
const isOpen = true;

test("modal shows the children and a close button", async () => {
  const handleClose = jest.fn();
  const { getByText } = render(
    <Modal isOpen={isOpen} character={null} onRequestClose={handleClose} />
  );

  const closeBtn = getByText(/Close/);
  expect(getByText(/Oops! No content to display/)).toBeTruthy();
  expect(closeBtn).toBeTruthy();
});

test("modal can close", async () => {
  const handleClose = jest.fn();
  const { getByText } = render(
    <Modal isOpen={isOpen} character={null} onRequestClose={handleClose} />
  );
  const closeBtn = getByText(/Close/);
  expect(closeBtn).toBeTruthy();

  fireEvent.click(closeBtn);
  expect(handleClose).toHaveBeenCalledTimes(1);
});
