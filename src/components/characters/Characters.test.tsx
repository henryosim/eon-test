import { MockedProvider } from "@apollo/client/testing";
import { render, screen, waitFor } from "@testing-library/react";
import { act } from "react-dom/test-utils";
import { GET_CHARACTERS } from "../queries";
import Characters from "./Characters";

const mockCharactersData = {
  request: {
    query: GET_CHARACTERS,
    variables: { page: 1 },
  },
  result: {
    data: {
      characters: {
        info: {
          count: 826,
          next: 2,
          pages: 42,
          prev: null,
          __typename: "Info",
        },
        results: [
          {
            __typename: "Character",
            id: "1",
            name: "Rick Sanchez",
            image: "https://rickandmortyapi.com/api/character/avatar/1.jpeg",
            status: "Alive",
            species: "Human",
            location: {
              __typename: "Location",
              name: "Citadel of Ricks",
            },
          },
        ],
        __typename: "Characters",
      },
    },
  },
};

const mockErrorData = {
  request: {
    query: GET_CHARACTERS,
    variables: { page: 1 },
  },
  error: new Error("Network Error!"),
};

test("renders Characters from api", async () => {
  const { getByText } = render(
    <MockedProvider mocks={[mockCharactersData]}>
      <Characters />
    </MockedProvider>
  );

  await act(() => waitFor(() => expect(getByText(/Rick/)).toBeInTheDocument()));
});

test("renders loading Text", () => {
  const { getByText } = render(
    <MockedProvider mocks={[mockCharactersData]}>
      <Characters />
    </MockedProvider>
  );

  expect(getByText("Loading...")).toBeInTheDocument();
});

test("renders with error", async () => {
  const { getByText } = render(
    <MockedProvider mocks={[mockErrorData]}>
      <Characters />
    </MockedProvider>
  );

  await act(async () => {
    await new Promise((resolve) => setTimeout(resolve, 0));
  });

  expect(getByText(/Loading Error!/)).toBeInTheDocument();
});
