import { useQuery } from "@apollo/client";
import React from "react";
import Character from "../character/Character";
import {
  CharacterDataInterface,
  CharacterInterface,
  CharacterVarsInterface,
  GET_CHARACTERS,
} from "../queries";
import Modal from "../modal/Modal";
import "./Characters.css";

export default function Characters() {
  const [modalIsOpen, setIsOpen] = React.useState<boolean>(false);
  const [page, setPage] = React.useState(1);
  const [character, setCharacter] = React.useState<CharacterInterface | null>(
    null
  );
  const { loading, error, data, fetchMore } = useQuery<
    CharacterDataInterface,
    CharacterVarsInterface
  >(GET_CHARACTERS, { variables: { page: page } });

  const info = { ...data?.characters.info };

  function openModal(item: CharacterInterface) {
    setCharacter(item);
    setIsOpen(true);
  }

  function closeModal() {
    setIsOpen(false);
  }

  function handleOnClickPrev() {
    const prevPage = info.prev;
    if (!prevPage) {
      return;
    }
    fetchMore({
      variables: { page: page },
    });
    setPage(prevPage);
  }

  async function handleOnClickNext() {
    const nextPage = await info.next;
    if (!nextPage) {
      return;
    }
    fetchMore({
      variables: { page: page },
    });
    setPage(nextPage);
  }

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Loading Error! {error.message}</p>;

  return (
    <>
      <div className="grid">
        {data?.characters?.results.map((character: CharacterInterface) => (
          <Character
            character={character}
            key={character.id}
            openModal={openModal}
          />
        ))}
      </div>
      <div className="pagination">
        {info.prev && <button onClick={() => handleOnClickPrev()}>Prev</button>}
        {page && <strong> | Page - {page} | </strong>}
        {info.next && <button onClick={() => handleOnClickNext()}>Next</button>}
      </div>
      <Modal
        character={character}
        isOpen={modalIsOpen}
        onRequestClose={closeModal}
      />
    </>
  );
}
