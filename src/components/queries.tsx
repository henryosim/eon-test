import { gql } from "@apollo/client";

export interface CharacterInterface {
  id: number;
  name: string;
  image: string;
  status: string;
  species: string;
  location: {
    name: string;
  };
}

export interface InfoInterface {
  next: number;
  prev: number;
  pages: number;
  count: number;
}

export interface CharacterDataInterface {
  characters: {
    info: InfoInterface;
    results: CharacterInterface[];
  };
}

export interface CharacterVarsInterface {
  page: number;
}

export const GET_CHARACTERS = gql`
  query Character($page: Int) {
    characters(page: $page) {
      info {
        count
        pages
        prev
        next
      }
      results {
        id
        name
        image
        status
        species
        location {
          name
        }
      }
    }
  }
`;
